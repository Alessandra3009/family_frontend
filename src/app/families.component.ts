import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {Father} from './Father';
import {Family} from './Family';

import {FatherService} from './father.service';

@Component({
  templateUrl: './families.component.html',
  providers: [FatherService]

})
export class FamiliesComponent implements OnInit {

    errorString:String;
    fathers:Father[]=[];
    visible:Boolean;
    searchBy:String;
    input:any;

    options= ["First name", "Second name", "PESEL", "Date of the birth", "Sex"];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private fatherService: FatherService) { 
          this.visible=false;
        }

        search(){
          switch(this.searchBy) { 
            case "First name": { 
              this.getFathersByChildFirstName();
               break; 
            } 
            case "Second name": { 
              this.getFathersByChildSecondName();
               break; 
            } 
            case "PESEL": {
              this.getFatherByChildPesel()
               break;    
            } 
            case "Date of the birth": { 
              this.getFatherByChildBirthDate();
               break; 
            }  
            case "Sex": { 
              this.getFatherByChildSex();
              break; 
           }  
         }
        }
        
        getFathers(){
          this.fatherService.getFathers().subscribe(result => this.fathers=result, error=>this.errorString=<any> error);
        }

        getFathersByChildFirstName(){
          this.visible=true;
          this.fatherService.getFatherByChildFirstName(this.input).subscribe(result => this.fathers=result, error=>this.errorString=<any> error);
        }

        getFathersByChildSecondName(){
          this.visible=true;
          this.fatherService.getFatherByChildSecondName(this.input).subscribe(result => this.fathers=result, error=>this.errorString=<any> error);
        }

        getFatherByChildPesel(){
          this.visible=true;
          this.fatherService.getFatherByChildPesel(this.input).subscribe(result => this.fathers=result, error=>this.errorString=<any> error);
        }

        getFatherByChildBirthDate(){
          this.visible=true;
          this.fatherService.getFatherByChildBirthDate(this.input).subscribe(result => this.fathers=result, error=>this.errorString=<any> error);
        }

        getFatherByChildSex(){
          this.visible=true;
          this.fatherService.getFatherByChildSex(this.input).subscribe(result => this.fathers=result, error=>this.errorString=<any> error);
        }

        chooseFamily(currentFamily :any){
          localStorage.setItem('currentFamily',JSON.stringify(currentFamily));
          this.router.navigate(['/familyOne'])
        }

   

        ngOnInit() {
}
}
