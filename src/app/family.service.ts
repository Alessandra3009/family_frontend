import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { RouterModule, Routes } from '@angular/router';
import { Headers, Response, RequestOptions, ResponseOptions } from '@angular/http';
import { map, take, catchError } from 'rxjs/operators';

import { Family } from './Family';

import * as URL_CONST from './url';

@Injectable()
export class FamilyService {

constructor(private http: Http) {};

    
getFamilies(): Observable<Family[]> {
     return this.sendGet(URL_CONST.FAMILIES_GET_);
   }

getFamily(id :number):Observable<Family>{
    return this.sendGet(URL_CONST.FAMILY_GET_ + id)
}

createFamily(FamilyData: any): Observable<any> {
   console.log('Create Family Service');
   return this.sendPost(URL_CONST.FAMILY_CREATE_, FamilyData);
}


deleteFamily(Family:any) {    
  return  this.sendPost(URL_CONST.FAMILY_DELETE_, Family )
 .subscribe(
       data => {
          console.log('The family is deleted');   
       },
       err => console.log(err),
       () => console.log('Request Complete')
       );     
}


public sendGet(URL: string): Observable<any> {
    return this.http.get(URL).pipe(
        map(this.extractData),
        catchError(this.handleError));
}

public sendPost(URL: string, body: any): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(URL, body, options).pipe(
        map(this.extractData),
        catchError(this.handleError));
}

private extractData(res: Response) {
    console.log(res);
    let body = res.json();
    return body || {};
}

private handleError(error: Response) {
    console.error('An error occurred', error);
    console.log(error.status)
    return Observable.throw(error.text || error);
}
}

