const BASIC_URL: string = "http://localhost:8080";

const FAMILIES_GET ="/families";
const FAMILY_GET="/family/";
const FAMILY_CREATE="/createFamily";
const FAMILY_DELETE="/deleteFamily";

const FATHERS_GET ="/fathers";
const FATHER_GET="/father/";
const FATHER_CREATE="/createFather";
const FATHER_DELETE="/deleteFather";
const FATHER_GET_BY_ID_FAMILY="/father/family-";
const FATHER_GET_BY_CHILD_FIRST_NAME="/fathers/child/firstName-";
const FATHER_GET_BY_CHILD_SECOND_NAME="/fathers/child/secondName-";
const FATHER_GET_BY_CHILD_PESEL="/fathers/child/pesel-";
const FATHER_GET_BY_CHILD_BIRTHDATE="/fathers/child/birthDate-";
const FATHER_GET_BY_CHILD_SEX="/fathers/child/sex-";

const CHILDREN_GET ="/children";
const CHILD_GET="/child/";
const CHILD_CREATE="/createChild";
const CHILD_DELETE="/deleteChild";
const CHILDREN_GET_BY_ID_FAMILY ="/children/family-";


export const FAMILIES_GET_ = BASIC_URL + FAMILIES_GET;
export const FAMILY_GET_ = BASIC_URL + FAMILY_GET;
export const FAMILY_CREATE_ = BASIC_URL + FAMILY_CREATE;
export const FAMILY_DELETE_ = BASIC_URL + FAMILY_DELETE;

export const FATHERS_GET_ = BASIC_URL + FATHERS_GET;
export const FATHER_GET_ = BASIC_URL + FATHER_GET;
export const FATHER_CREATE_ = BASIC_URL + FATHER_CREATE;
export const FATHER_DELETE_ = BASIC_URL + FATHER_DELETE;
export const FATHER_GET_BY_ID_FAMILY_=BASIC_URL + FATHER_GET_BY_ID_FAMILY;
export const FATHER_GET_BY_CHILD_FIRST_NAME_=BASIC_URL + FATHER_GET_BY_CHILD_FIRST_NAME;
export const FATHER_GET_BY_CHILD_SECOND_NAME_=BASIC_URL + FATHER_GET_BY_CHILD_SECOND_NAME;
export const FATHER_GET_BY_CHILD_PESEL_=BASIC_URL + FATHER_GET_BY_CHILD_PESEL;
export const FATHER_GET_BY_CHILD_BIRTHDATE_=BASIC_URL + FATHER_GET_BY_CHILD_BIRTHDATE;
export const FATHER_GET_BY_CHILD_SEX_=BASIC_URL + FATHER_GET_BY_CHILD_SEX;

export const CHILDREN_GET_ = BASIC_URL + CHILDREN_GET;
export const CHILD_GET_ = BASIC_URL + CHILD_GET;
export const CHILD_CREATE_ = BASIC_URL + CHILD_CREATE;
export const CHILD_DELETE_ = BASIC_URL + CHILD_DELETE;
export const CHILDREN_GET_BY_ID_FAMILY_=BASIC_URL + CHILDREN_GET_BY_ID_FAMILY;
















const BASIC_AUTH: string = "/auth";
const LOGIN: string = "/student/login";
const LOGINT: string = "/teacher/login";
const LOGGED_USER: string = "/user/logged";
const LOGOUT: string = "/user/logout";
const REGISTER: string = "/user/register";
const REST_HAIRDRESSERS: string = "/rest/hairdressers";
const AVAILABLE_SERVICES: string = "/rest/services/available";
const SERVICES: string = "/services";
const VISIT_PROPOSALS: string = "/rest/visits/";
const VISIT_AVAILABLE: string = "/available/";
const VISIT_DATE: string = "?date=";
const REST: string = "/rest";
const VISITS: string = "/visits";
const VISIT_RESERVE: string = "/reserve";
const VISIT_CANCEL: string = "/cancel/";
const OPINIONS = "/opinions";
const OPINIONS_COUNT = "/rest/opinions/";
const SHOW = "/show";
const HIDE = "/hide";
const UPGRADE = "/upgrade";
const HAIRDRESSERS = "/hairdressers";
const CHECK = "/check";
const USERS = "/users";
const USERNAME = "?username=";
const UPCOMING = "/upcoming";
const HISTORY = "/history";
const TICKETS = "/tickets";
const DISCOUNTS ="/discounts";
const HALLS ="/halls";
const EXAMS ="/exams";
const MATERIALS ="/materials";
const LECTURES ="/lectures";
const COURSES ="/courses";
const STUDENTS ="/students";
const TEACHERS ="/teachers";
const NEWSALL ="/news_all";
const ROWS="/rows";
const ARTISTS="/artists";
const CONCERTS="/concerts";
const PERFORMANCES="/performances";
const TEACHINGS="/teachings";
const PLACES="/places";
const EVENTS="/events";
const RESERVATIONS="/reservations";
const SYMPHONY_CONCERTS="/concert/rodzaj-Symfoniczny";
const EVENTS_SYMPHONY_CONCERT="/event/concert-Symfoniczny";
const EVENTS_CHAMBER_CONCERT="/event/concert-Kameralny";
const EVENTS_QUARTET_CONCERT="/event/concert-Kwartet";
const EVENTS_CHORAL_CONCERT="/event/concert-Chóralny";
const CONCERT="/concert/";
const EVENT="/event/concert-one-";
const EVENT_ID="/event/";
const REGISTER_USER="/user";
const REGISTER_STUDENT="/student";
const REGISTER_TEACHER="/teacher";
const UPDATE_TEACHER="/teacher/update";
const PIECES="/pieces";
const PIECE_PERFORMANCES="/pieceperformances";
const ROW_1="/place/rzad-1";
const ROW_2="/place/rzad-2";
const ROW_3="/place/rzad-3";
const ROW_4="/place/rzad-4";
const ROW_5="/place/rzad-5";
const ROW_6="/place/rzad-6";
const ROW_7="/place/rzad-7";
const ROW_8="/place/rzad-8";
const ROW_9="/place/rzad-9";
const PLACE_ID="/place/";
const USER_EMAIL="/user/email-";
const STUDENT_EMAIL="/student/email-";
const TEACHER_EMAIL="/teacher/email-";
const USER_ID="/user/";
const STUDENT_ID="/student/";
const TEACHER_ID="/teacher/";
const EXAM_ID="/exam/";
const CREATE_RESERVATION="/reservation";
const RESERVATION_ID="/reservation/";
const LECTURE_ID="/lecture/";
const EMAIL_SENDER="/email";
const DISCOUNT_ID="/discount/";
const CREATE_TICKET="/ticket";
const CREATE_EXAM="/createExam";
const CREATE_LECTURE="/createLecture";
const CREATE_NEWS="/createNews";
const CREATE_MATERIAL="/createMaterial";
const TICKET_ID="/ticket/";
const MATERIAL_ID="/material/";
const RESERVATION_DELETE="/reservation/delete/";
const LECTURE_DELETE="/lecture/delete/";
const TICKET_DELETE="/ticket/delete/";
const EXAM_DELETE="/exam/delete/";
const NEWS_DELETE="/news/delete/";
const MATERIAL_DELETE="/material/delete/";
const EMAIL_SENDER_CANCEL="/cancel";
const EMAIL_SENDER_REGISTER="/emailRegister";

export const EMAIL_SENDER_CANCEL_ALL=BASIC_URL+EMAIL_SENDER_CANCEL;
export const EMAIL_SENDER_REGISTER_ALL=BASIC_URL+EMAIL_SENDER_REGISTER;
export const TICKET_DELETE_ID=BASIC_URL+TICKET_DELETE;
export const EXAM_DELETE_ID=BASIC_URL+EXAM_DELETE;
export const NEWS_DELETE_ID=BASIC_URL+NEWS_DELETE;
export const MATERIAL_DELETE_ID=BASIC_URL+MATERIAL_DELETE;
export const RESERVATION_DELETE_ID=BASIC_URL+RESERVATION_DELETE;
export const LECTURE_DELETE_ID=BASIC_URL+LECTURE_DELETE;
export const TICKET_ID_ONE=BASIC_URL+TICKET_ID;
export const MATERIAL_ID_ONE=BASIC_URL+MATERIAL_ID;
export const CREATE_TICKET_ALL=BASIC_URL+ CREATE_TICKET;
export const CREATE_EXAM_ALL=BASIC_URL+ CREATE_EXAM;
export const CREATE_LECTURE_ALL=BASIC_URL+ CREATE_LECTURE;
export const CREATE_NEWS_ALL=BASIC_URL+ CREATE_NEWS;
export const CREATE_MATERIAL_ALL=BASIC_URL+ CREATE_MATERIAL;
export const DISCOUNT_ID_ONE=BASIC_URL+DISCOUNT_ID;
export const EMAIL_SENDER_ALL=BASIC_URL+EMAIL_SENDER;
export const RESERVATION_ID_ONE=BASIC_URL+RESERVATION_ID;
export const LECTURE_ID_ONE=BASIC_URL+LECTURE_ID;
export const CREATE_RESERVATION_ALL=BASIC_URL+ CREATE_RESERVATION;
export const USER_ID_ONE=BASIC_URL+USER_ID;
export const STUDENT_ID_ONE=BASIC_URL+STUDENT_ID;
export const TEACHER_ID_ONE=BASIC_URL+TEACHER_ID;
export const EXAM_ID_ONE=BASIC_URL+EXAM_ID;
export const USER_EMAIL_ONE=BASIC_URL+USER_EMAIL;
export const STUDENT_EMAIL_ONE=BASIC_URL+STUDENT_EMAIL;
export const TEACHER_EMAIL_ONE=BASIC_URL+TEACHER_EMAIL;
export const PLACE_ID_ONE=BASIC_URL+PLACE_ID;
export const ROW_1_PLACES= BASIC_URL+ROW_1;
export const ROW_2_PLACES= BASIC_URL+ROW_2;
export const ROW_3_PLACES= BASIC_URL+ROW_3;
export const ROW_4_PLACES= BASIC_URL+ROW_4;
export const ROW_5_PLACES= BASIC_URL+ROW_5;
export const ROW_6_PLACES= BASIC_URL+ROW_6;
export const ROW_7_PLACES= BASIC_URL+ROW_7;
export const ROW_8_PLACES= BASIC_URL+ROW_8;
export const ROW_9_PLACES= BASIC_URL+ROW_9;
export const PIECE_PERFORMANCE_ALL=BASIC_URL+PIECE_PERFORMANCES;
export const PIECES_ALL=BASIC_URL+PIECES;
export const LOGIN_USER=BASIC_URL+LOGIN;
export const LOGIN_STUDENT=BASIC_URL+LOGIN;
export const LOGIN_TEACHER=BASIC_URL+LOGINT;
export const LOGOUT_USER=BASIC_URL+LOGOUT;
export const REGISTER_USER_ALL=BASIC_URL+REGISTER_USER;
export const REGISTER_STUDENT_ALL=BASIC_URL+REGISTER_STUDENT;
export const REGISTER_TEACHER_ALL=BASIC_URL+REGISTER_TEACHER;
export const UPDATE_TEACHER_ALL=BASIC_URL+UPDATE_TEACHER;
export const EVENT_ID_ONE=BASIC_URL+EVENT_ID;
export const EVENT_ONE=BASIC_URL + EVENT;
export const CONCERT_ONE=BASIC_URL + CONCERT;
export const EVENT_CHORAL_CONCERT_ALL = BASIC_URL + EVENTS_CHORAL_CONCERT;
export const EVENT_QUARTET_CONCERT_ALL = BASIC_URL + EVENTS_QUARTET_CONCERT;
export const EVENT_CHAMBER_CONCERT_ALL = BASIC_URL + EVENTS_CHAMBER_CONCERT;
export const EVENT_SYMPHONY_CONCERT_ALL = BASIC_URL + EVENTS_SYMPHONY_CONCERT;
export const SYMPHONY_CONCERT_ALL = BASIC_URL + SYMPHONY_CONCERTS;
export const TICKET_ALL = BASIC_URL + TICKETS;
export const RESERVATION_ALL = BASIC_URL + RESERVATIONS;
export const EVENT_ALL = BASIC_URL + EVENTS;
export const PLACE_ALL = BASIC_URL + PLACES;
export const NEWS_ALL = BASIC_URL + NEWSALL;
export const PERFORMANCE_ALL = BASIC_URL + PERFORMANCES;
export const TEACHING_ALL = BASIC_URL + TEACHINGS;
export const CONCERT_ALL = BASIC_URL + CONCERTS;
export const ARTIST_ALL = BASIC_URL + ARTISTS;
export const ROW_ALL = BASIC_URL + ROWS;
export const DISCOUNT_ALL = BASIC_URL + DISCOUNTS;
export const HALL_ALL= BASIC_URL + HALLS;
export const EXAM_ALL= BASIC_URL + EXAMS;
export const MATERIAL_ALL= BASIC_URL + MATERIALS;
export const COURSE_ALL= BASIC_URL + COURSES;
export const LECTURE_ALL= BASIC_URL + LECTURES;
export const USER_ALL = BASIC_URL + USERS;
export const STUDENT_ALL = BASIC_URL + STUDENTS;
export const TEACHER_ALL = BASIC_URL + TEACHERS;
export const HOME_URL = BASIC_URL + '/';
export const HAIRDRESSERS_URL = BASIC_URL + REST_HAIRDRESSERS;
export const AVAILABLE_SERVICES_URL = BASIC_URL + AVAILABLE_SERVICES;
export const SERVICES_URL = BASIC_URL + BASIC_AUTH + SERVICES;
export const VISIT_PROPOSALS_URL = function (hairdresserId: number, serviceId: number, date: string) {
    return BASIC_URL + VISIT_PROPOSALS + hairdresserId + VISIT_AVAILABLE + serviceId + VISIT_DATE + date;
}
export const VISIT_RESERVE_URL = BASIC_URL + REST + VISITS + VISIT_RESERVE;
export const LOGIN_URL = BASIC_URL + LOGIN;
export const LOGGED_USER_URL = BASIC_URL + BASIC_AUTH + LOGGED_USER;
export const LOGOUT_URL = BASIC_URL + LOGOUT;
export const REGISTER_URL = BASIC_URL + REGISTER;
export const OPINIONS_URL = BASIC_URL + BASIC_AUTH + OPINIONS;
export const OPINIONS_CLIENT_URL = BASIC_URL + BASIC_AUTH + OPINIONS + USERS + "/";
export const OPINIONS_COUNT_URL = BASIC_URL + OPINIONS_COUNT;
export const VISITS_URL = BASIC_URL + BASIC_AUTH + VISITS;
export const VISITS_PARAM_URL = BASIC_URL + BASIC_AUTH + VISITS + "/";
export const VISITS_PARAM_UPCOMING_URL = BASIC_URL + BASIC_AUTH + VISITS + UPCOMING + "/";
export const VISITS_PARAM_HISTORY_URL = BASIC_URL + BASIC_AUTH + VISITS + HISTORY + "/";
export const VISITS_CANCEL_URL = BASIC_URL + BASIC_AUTH + VISITS + VISIT_CANCEL;
export const SERVICES_ADD = BASIC_URL + BASIC_AUTH + SERVICES + "/";
export const SERVICES_SHOW = BASIC_URL + BASIC_AUTH + SERVICES + SHOW + "/";
export const SERVICES_HIDE = BASIC_URL + BASIC_AUTH + SERVICES + HIDE + "/";
export const UPGRADE_HAIDRESSER = BASIC_URL + BASIC_AUTH + HAIRDRESSERS + UPGRADE + USERNAME;
export const CHECK_EMAIL = BASIC_URL + BASIC_AUTH + USERS + CHECK + USERNAME;




