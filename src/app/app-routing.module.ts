import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddFatherComponent} from './addFather.component';
import { AppComponent} from './app.component';
import { DashboardComponent} from './dashboard.component';
import { AddChildComponent} from './addChild.component';
import { FamilyOneComponent} from './familyOne.component';
import { FamiliesComponent} from './families.component';



const routes: Routes = [
  { path: '',
  redirectTo: '/start',
  pathMatch: 'full'
},
{
  path: 'start',
  component: DashboardComponent
},
  { 
    path: 'addFather', 
    component: AddFatherComponent 
  },
  { 
    path: 'addChild', 
    component: AddChildComponent 
  },
  { 
    path: 'familyOne', 
    component: FamilyOneComponent 
  },
  { 
    path: 'families', 
    component: FamiliesComponent 
  }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})


export class AppRoutingModule {}