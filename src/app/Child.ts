import {Family} from './Family';
import {Sex} from './sex.enum';


export class Child{
    constructor(
                public idChild?:number, public firstName?:String, 
                public secondName?:String, public pesel?:String, 
                public sex?: Sex, public birthDate?:Date,
                public family?:Family
                ){}
}

