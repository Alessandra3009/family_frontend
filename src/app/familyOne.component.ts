import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {Father} from './Father';
import {Child} from './Child';
import {Family} from './Family';

import {FatherService} from './father.service';
import {ChildService} from './child.service';

@Component({
  templateUrl: './familyOne.component.html',
  providers: [FatherService, ChildService]

})
export class FamilyOneComponent implements OnInit {

    errorString:String;
    idFamily:number;
    currentFamily:Family=new Family();
    father:Father=new Father()
    children:Child[]=[];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private fatherService: FatherService,  private childService: ChildService) { }

        getFamily(){
          this.currentFamily=JSON.parse(localStorage.getItem('currentFamily'));
          this.idFamily=this.currentFamily.idFamily;
        }

        getFather(){
          this.fatherService.getFatherByFamilyIdFamily(this.idFamily).subscribe(result => this.father=result, error=>this.errorString=<any> error);
        }

        getChildren(){
          this.childService.getChildrenByFamilyIdFamily(this.idFamily).subscribe(result => this.children=result, error=>this.errorString=<any> error);
        }


        ngOnInit() {
          this.getFamily();
          this.getFather();
          this.getChildren();
}
}
