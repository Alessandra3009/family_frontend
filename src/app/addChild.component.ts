import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Sex } from './sex.enum';
import {Child} from './Child';
import {Family} from './Family';
import {Father} from './Father';
import {FamilyService} from './family.service';
import {FatherService} from './father.service';
import {ChildService} from './child.service';
import {AlertService} from './Alert.service';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './addChild.component.html',
  providers: [FamilyService, AlertService, FatherService, ChildService]

})
export class AddChildComponent implements OnInit {

    loading = false;
    child:Child=new Child();
    children:Child[]=[];
    sexs:String[]=[];
    sex_:String;
    family:Family= new Family();
    father:Father= new Father();
    currentFamily:Family=new Family();
   
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private familyService: FamilyService, private alertService: AlertService,
        private fatherService: FatherService,  private childService: ChildService) {
         }

        getChildren(){
            this.children=JSON.parse(localStorage.getItem('addedChildren'));
        }

        getFather(){
            this.father=JSON.parse(localStorage.getItem('addedFather'));
        }

        addChild(){
           if(this.sex_==Sex[Sex.Woman])
            {this.child.sex=Sex.Woman}
           else{this.child.sex=Sex.Man}

            this.children.push(this.child);
            localStorage.setItem('addedChildren', JSON.stringify(this.children));
        }

         createFamily() {
            this.loading = true;
            return this.familyService.createFamily(this.family)
            .subscribe(
                data => {
                        let currentFamily=data;
                        console.log('The family is created ' + currentFamily);
                        localStorage.setItem('currentFamily',JSON.stringify(currentFamily));
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                },
                () => {
                    this.createFather(); 
                });
    }

          createFather(){
            this.currentFamily=JSON.parse(localStorage.getItem('currentFamily'));
            this.father.family=this.currentFamily;
            return this.fatherService.createFather(this.father)
            .subscribe(
                data => {
                    console.log('The father is created'); 
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                },
                () => { 
                    this.createChildren();
                });
          }

          moreChild() {
           this.addChild();
           window.location.reload();
          }

          createChildren(){
            this.currentFamily=JSON.parse(localStorage.getItem('currentFamily'));
            for(let newChild of this.children){
                newChild.family=this.currentFamily;
               this.childService.createChild(newChild);
            }
            this.router.navigate(['/familyOne']);
          }


        ngOnInit() {
            this.getChildren();
            this.getFather();
            for (let item in Sex) {
             if (isNaN(Number(item))) {
                this.sexs.push(item);
             }
        }
}
}