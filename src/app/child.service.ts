import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { RouterModule, Routes } from '@angular/router';
import { Headers, Response, RequestOptions, ResponseOptions } from '@angular/http';
import { map, take, catchError } from 'rxjs/operators';

import { Child } from './Child';

import * as URL_CONST from './url';

@Injectable()
export class ChildService {

constructor(private http: Http) {};

getChildren(): Observable<Child[]> {
    return this.sendGet(URL_CONST.CHILDREN_GET_);
}

getChild(id :number):Observable<Child>{
    return this.sendGet(URL_CONST.CHILD_GET_ + id)
}

getChildrenByFamilyIdFamily(id :number): Observable<Child[]> {
    return this.sendGet(URL_CONST.CHILDREN_GET_BY_ID_FAMILY_ + id)
}

createChild(ChildData: any) {
    console.log('Create Child Service');
    return this.sendPost(URL_CONST.CHILD_CREATE_, ChildData)
    .subscribe(
     data => {
        console.log('The child is created');   
     },
     err => console.log(err),
     () => console.log('Request Complete')
     );  
 }

deleteChild(Child:any) {    
  return  this.sendPost(URL_CONST.CHILD_DELETE_, Child )
 .subscribe(
       data => {
          console.log('The child is deleted');   
       },
       err => console.log(err),
       () => console.log('Request Complete')
       );     
}


public sendGet(URL: string): Observable<any> {
    return this.http.get(URL).pipe(
        map(this.extractData),
        catchError(this.handleError));
}

public sendPost(URL: string, body: any): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(URL, body, options).pipe(
        map(this.extractData),
        catchError(this.handleError));
}

private extractData(res: Response) {
    console.log(res);
    let body = res.json();
    return body || {};
}

private handleError(error: Response) {
    console.error('An error occurred', error);
    console.log(error.status)
    return Observable.throw(error.text || error);
}
}

