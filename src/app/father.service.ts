import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { RouterModule, Routes } from '@angular/router';
import { Headers, Response, RequestOptions, ResponseOptions } from '@angular/http';
import { map, take, catchError } from 'rxjs/operators';

import { Father } from './Father';

import * as URL_CONST from './url';

@Injectable()
export class FatherService {

constructor(private http: Http) {};

getFathers(): Observable<Father[]> {
    return this.sendGet(URL_CONST.FATHERS_GET_);
}

getFather(id :number):Observable<Father>{
    return this.sendGet(URL_CONST.FATHER_GET_ + id)
}

getFatherByFamilyIdFamily(id :number):Observable<Father>{
    return this.sendGet(URL_CONST.FATHER_GET_BY_ID_FAMILY_ + id)
}

getFatherByChildFirstName(data :any):Observable<Father[]>{
    return this.sendGet(URL_CONST.FATHER_GET_BY_CHILD_FIRST_NAME_ + data)
}

getFatherByChildSecondName(data :any):Observable<Father[]>{
    return this.sendGet(URL_CONST.FATHER_GET_BY_CHILD_SECOND_NAME_ + data)
}

getFatherByChildPesel(data :any):Observable<Father[]>{
    return this.sendGet(URL_CONST.FATHER_GET_BY_CHILD_PESEL_ + data)
}

getFatherByChildBirthDate(data :any):Observable<Father[]>{
    return this.sendGet(URL_CONST.FATHER_GET_BY_CHILD_BIRTHDATE_ + data)
}

getFatherByChildSex(data :any):Observable<Father[]>{
    return this.sendGet(URL_CONST.FATHER_GET_BY_CHILD_SEX_ + data)
}

createFather(FatherData: any): Observable<any> {
    console.log('Create Father Service');
    return this.sendPost(URL_CONST.FATHER_CREATE_, FatherData); 
 }

deleteFather(Father:any) {    
  return  this.sendPost(URL_CONST.FATHER_DELETE_, Father )
 .subscribe(
       data => {
          console.log('The father is deleted');   
       },
       err => console.log(err),
       () => console.log('Request Complete')
       );     
}


public sendGet(URL: string): Observable<any> {
    return this.http.get(URL).pipe(
        map(this.extractData),
        catchError(this.handleError));
}

public sendPost(URL: string, body: any): Observable<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    //headers.append('Authorization', 'Bearer ' + localStorage.getItem('id_token'))
    let options = new RequestOptions({ headers: headers });
    return this.http.post(URL, body, options).pipe(
        map(this.extractData),
        catchError(this.handleError));
}

private extractData(res: Response) {
    console.log(res);
    let body = res.json();
    return body || {};
}

private handleError(error: Response) {
    console.error('An error occurred', error);
    console.log(error.status)
    return Observable.throw(error.text || error);
}
}

