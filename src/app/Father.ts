import {Family} from './Family';


export class Father{
    constructor(
                public idFather?:number, public firstName?:String, 
                public secondName?:String, public birthDate?:Date,
                public pesel?:String, public family?:Family
                ){}
}