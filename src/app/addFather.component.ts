import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {Father} from './Father';
import {Child} from './Child';

@Component({
  templateUrl: './addFather.component.html',

})
export class AddFatherComponent implements OnInit {

    father:Father=new Father();
    public addedChildren:Child[]=[];

    constructor(
        private route: ActivatedRoute,
        private router: Router) { }

        addFather(){
          localStorage.setItem('addedFather', JSON.stringify(this.father));
          localStorage.setItem('addedChildren', JSON.stringify(this.addedChildren));
          this.router.navigate(['/addChild']);
        }

        ngOnInit() {
        }
}
