import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AddFatherComponent } from './addFather.component';
import { AddChildComponent } from './addChild.component';
import { FamilyOneComponent } from './familyOne.component';
import { FamiliesComponent } from './families.component';
import { NavbarComponent } from './navbar.component';
import { DashboardComponent } from './dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent, 
    AddFatherComponent,
    NavbarComponent,
    DashboardComponent,
    AddChildComponent,
    FamilyOneComponent,
    FamiliesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    RouterModule,
    FormsModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
